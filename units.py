"""
Realistic simulation of a growing Arabidopsis architecture based of GRN regulation

Simulations associated with the paper:
  How genetic perturbations change flowers into fractal-like cauliflower
  by Azpeitia et al. 2021.

Authors: Christophe Godin and Eugenio Azpeitia
Design contributions: Francois Parcy, Etienne Farcot
Licence: Open source LGPL
"""
##################
# Model variables
##################

# Time is counted in hours in the model
day = 24.                # constant defined for convenience in hours

T = 10.                  # plastochrone in hours

dt = 1.                  # in hours. Unique parameter to change
                         # time dicretization (this should hardly affect
                         # simulations beyond integration approximations)

cm = 1.                  # 1 cm = 1 PlantGl grid unit
