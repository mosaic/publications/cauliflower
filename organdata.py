"""
Realistic simulation of a growing Arabidopsis architecture based of GRN regulation

Simulations associated with the paper:
  How genetic perturbations change flowers into fractal-like cauliflower
  by Azpeitia et al. 2021.

Authors: Christophe Godin and Eugenio Azpeitia
Design contributions: Francois Parcy, Etienne Farcot
Licence: Open source LGPL
"""
from units import *

#########################
# Organs
##########################


# Internode
InternodeSize = 1.                # scaling coefficient, to adapt the model petal size to the plant size

InternodeInitialLength = 0.01*cm
InternodeInitialDiam = 0.05*cm   # changed: initially was 0.1
InternodeInitialWidth = 0.1*cm
InternodeInitialOpeningAngle = 0  # (in degrees)

InternodeFinalLength = 3.*cm
InternodeFinalDiam = 0.3*cm
InternodeFinalWidth = 0.1*cm
InternodeFinalOpeningAngle = 0   # (in degrees)

InternodeColor = 6

InternodeDelay = 1.0*T       #  10*T
InternodeLifespan = 50*day    # 50 days
InternodeHalfLife = 15*day    # 15 days
InternodeStiffness = 3. #1.


# Leaf
petiol_prop = 0.3
leafsize = 25.*cm

# Pedicel
PedicelSize = 1.                # scaling coefficient, to adapt the model petal size to the plant size

PedicelInitialLength = 0.01*cm
PedicelInitialDiam = 0.01*cm
PedicelInitialWidth = 0.1*cm
PedicelInitialOpeningAngle = 0  # (in degrees)

PedicelFinalLength = 7*cm
PedicelFinalDiam = 0.15*cm
PedicelFinalWidth = 0.1*cm
PedicelFinalOpeningAngle = 0   # (in degrees)

PedicelColor = 2

PedicelDelay = 0.0*day
PedicelLifespan = 50*day
PedicelHalfLife = 15*day
PedicelStiffness = 2.

# Sepals
SepalSize = 1.0                # scaling coefficient, to adapt the petal size to the plant size

SepalInitialLength = 1*cm
SepalInitialDiam = 0.20*cm
SepalInitialWidth = 3*cm
SepalInitialOpeningAngle = 5  # (in degrees)

SepalFinalLength = 2*cm
SepalFinalDiam = 0.20*cm
SepalFinalWidth = 5*cm
SepalFinalOpeningAngle = 15   # (in degrees)

SepalColor = 2

SepalDelay = 0.0*day
SepalLifespan = 14.5*day
SepalHalfLife = 10*day
SepalStiffness = 2.

# Petals
PetalSize = 1.0                # scaling coefficient, to adapt the petal size to the plant size

PetalInitialLength = 0.7*cm
PetalInitialDiam = 0.15*cm
PetalInitialWidth = 2*cm
PetalInitialOpeningAngle = 10  # (in degrees)

PetalFinalLength = 4*cm
PetalFinalDiam = 0.15*cm
PetalFinalWidth = 6*cm
PetalFinalOpeningAngle = 10   # (in degrees)

PetalColor = 0

PetalDelay = 0.0*day
PetalLifespan = 14.5*day
PetalHalfLife = 15*day
PetalStiffness = 4.

# Carpels
CarpelSize = 1                  # scaling coefficient, to adapt the model petal size to the plant size

CarpelInitialLength = 0.7*cm
CarpelInitialDiam = 0.01*cm
CarpelInitialWidth = 0.1*cm
CarpelInitialOpeningAngle = 0  # (in degrees)

CarpelFinalLength = 9*cm
CarpelFinalDiam = 0.5*cm
CarpelFinalWidth = 0.3*cm
CarpelFinalOpeningAngle = 0   # (in degrees)

CarpelColor = 6

CarpelDelay = 5.0*day
CarpelLifespan = 50*day
CarpelHalfLife = 10*day
CarpelStiffness = 10.
