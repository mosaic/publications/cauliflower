This file explains the steps to install and run the simulations used in the article 
'Cauliflower fractal forms arise from perturbations of floral gene networks' 
by Azpeitia et al., 2021.

To run the GRN, geometric and the 3D architectural-GRN models it is necessary to install first some python packages. The procedure has been automated using CONDA. Here is how to proceed.

######################
 Installation
######################

STEP 0: (Preliminary) please install the CONDA system on your system:
(https://conda.io/projects/conda/en/latest/user-guide/install)
Open a command line shell on your system, and go in the directory where you downloaded the simulation files.

STEP 1: Install the programming environment using the conda file 
cauliflower.yml and the conda command:

$ conda env create -f cauliflower.yml -n cauliflower

This installs the required python packages and creates a conda environment. Here the conda environment's name is chosen to be 'cauliflower' via option '-n cauliflower')

STEP 2: Then activate the new environment:

$ conda activate cauliflower


You are ready to run the simulations.

######################
 Runing the code
######################

All the architectural simulations are carried out in the lpy simulation software. 
(https://lpy.readthedocs.io/en/latest)
You can launch the lpy simulation software using the command in the cauliflower environment:

$ lpy

3D architectural-GRN model:
---------------------------

*** WT simulation:

1) In L-py open the file arabidopsis-grn.lpy

2) Click on Run button (top button bar). 
By default, this runs a simulation of a WT growth over 28 days and displays the output.
Alternatively, click on Animate to see the intermediate steps of the simulation 

Note: to simulate the WT, check that all the boolean variables s_mut, a_mut, l_mut, t_mut and lfy_hetero as well as ROMANESCO (lines 73 to 79) are set to False.

*** Mutant simulations:

In lines 73 to 77 of the file arabidopsis-grn.lpy change the value of desired mutant from False to True. s_mut, a_mut, l_mut, t_mut and lfy_hetero stand for sax mutant, ap1 cal mutant, lfy mutant, tfl1 mutant and lfy+/ mutant, respectively.

*** Romanesco simulation:

Set a_mut to True (for ap1 mutant, line 74) and variable ROMANESCO to True (line 79) to produce the arabidopsis romancesco phenotype.

Geometric model:
----------------

1) In L-py open the file geometric-model.lpy

2) click on run.

*** Cauliflower and romancesco simulation:

On the scalar parameters click on the desired phenotype.

GRN model:
----------

The GRN model is contained in the network.py file. It can be run as a normal python file.


