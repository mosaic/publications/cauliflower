#! /usr/bin/env python

#########
#GRN simulation
########

"""
Realistic simulation of a growing Arabidopsis architecture based of GRN regulation

Simulations associated with the paper:
  How genetic perturbations change flowers into fractal-like cauliflower
  by Azpeitia et al. 2021.

Authors: Eugenio Azpeitia
Design contributions: Etienne Farcot, François Parcy and Christophe Godin
Licence: Open source LGPL
"""

import numpy as np
from scipy.integrate import odeint
from copy import deepcopy
import matplotlib.pyplot as plt

#HILL EXPONENT
n = 2#Strength of the induction of LFY by SAS

#DEGRADATIONS
deg = 0.1

#Affinities
#LFY Regulations:
kSL = 6.#this parameter changes the threshold for the flower transition in the mutant tfl1
kTL = .5#this parameter changes the threshold for the flower transition in the wt plant
kAL = 4.0
kLL = 100.
kauxL = 1.

lfy_weakened = 0.8

#AP1 Regulations
kLA = 1.0
kTA = 1.
kFA = 50.0
kSA = 15.0

#SAS Regulations
kAS = 4.
kFS = 10.
kSS = 20.

#TFL1 Regulations
kST = 1.
kLT = 100.0
kSAT = 1.
kRT = .1

#DIMERIZATION AND DISSOSIATION STRNEGTHS
kDimAP1SAS=0.1
kDisAP1SAS=0.01

#BASAL AUXIN ACTIVITY
bauxL = np.sqrt(0.1)#Independent LFY induction by Auxin

defpars = np.array( [ ] )

def dX_dt(X, t=0, pars=defpars):
        L,A,S,SA,T,aux,R=X
        F,ap1mutant,lfymutant,sasmutant,tfl1mutant,lfy_hetero = pars
        if lfymutant:
            lfy_ODE = 0
        elif lfy_hetero:
            lfy_ODE  = ((kTL**n/(T**n+kTL**n)))*(lfy_weakened*(((aux+bauxL)/(aux+bauxL+kauxL))*(kLL**n/(L**n+kLL**n))*(S**n/(S**n+kSL**n))+(A**n/(A**n+kAL**n))))-deg*L
        else:
            lfy_ODE  = ((kTL**n/(T**n+kTL**n)))*(((aux+bauxL)/(aux+bauxL+kauxL))*(kLL**n/(L**n+kLL**n))*(S**n/(S**n+kSL**n))+(A**n/(A**n+kAL**n)))-deg*L
        if ap1mutant:
            ap1_ODE = 0
        else:
            ap1_ODE  = (kTA**n/(T**n+kTA**n))*((L**n/(L**n+kLA**n))+(S**n/(S**n+kSA**n))*(F**n/(F**n+kFA**n)))+((-kDimAP1SAS*A*S)+(kDisAP1SAS*SA))-deg*A
        if sasmutant:
            sas_ODE = 0
        else:
            sas_ODE  = (kAS**n/(A**n+kAS**n))*((F/(F+kFS))+(S**n/(S**n+kSS**n)))+((-kDimAP1SAS*A*S)+(kDisAP1SAS*SA))-deg*S
        if tfl1mutant:
            tfl1_ODE = 0
        else:
            tfl1_ODE  = ((S**n/(S**n+kST**n))*((kRT/(kRT+R))*(kSAT**n/(SA**n+kSAT**n))))+(L**n/(L**n+kLT**n))-deg*T
        return np.array ([lfy_ODE,
                       ap1_ODE,
                       sas_ODE,
                       ((kDimAP1SAS*A*S)-(kDisAP1SAS*SA))-deg*SA,
                       tfl1_ODE,
                       -deg*aux,
                       -deg*R])

def sim_grn(X0,F,a_mut,l_mut,s_mut,t_mut,lfy_hetero):#X0 = initial state, F = F value, x_mut = 1 for mutant
        t = np.linspace(0,10,100)
        params = np.array( [F,a_mut,l_mut,s_mut,t_mut,lfy_hetero] )
        X_updated = np.zeros(X0.shape)
        th_dif = 0.000001
        max_dif = th_dif+1
        m = 0
        max_tries = 1000
        LFYmax = 0.
        while max_dif > th_dif and m<max_tries:
                m+=1
                X = odeint(dX_dt,X0,t,(params,))
                X_updated = X[-1]
                max_dif=np.max(np.absolute(X0-X_updated))
                X0 = deepcopy(X_updated)
                LFYmax = max(np.max(X[:,0]),LFYmax)
        if m==max_tries:
                print ('a steady state might not have been reached')
        return X[-1], LFYmax
